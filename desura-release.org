#+TITLE: 2x0ng on Desura

** Procedural indie puzzler 2x0ng brings new-style arcade action to Desura this Friday, July 12

*July 9, 2013* --- We are pleased to announce the impending release of
2x0ng on the Desura digital game distribution platform. 2x0ng will be
available for Windows and Linux this Friday, July 12, 2013; a Mac
version is in preparation.

file:2x0ng1.png

*** About the game

2x0ng is a procedural puzzle combat roguelike game rendered in an
8-bit retro style. Pronounce it "TWO-zong". You control a little
pixel-person running through a maze; with your bouncing,
color-changing Squareball, you can match colors to unlock new areas,
and destroy an onslaught of enemies as you make your way to the exit.

Previous versions of 2x0ng were featured earlier this year on Indie
Impressions, Tigsource, Indie Statik, Destructoid, Free Indie Games,
Free Gamer, and GamingOnLinux, among others.

2x0ng is [[http://en.wikipedia.org/wiki/Free_software][Free Software]], with its Common Lisp source code distributed
under the terms of the [[http://en.wikipedia.org/wiki/GNU_General_Public_License][GNU GPL Version 3]] and media assets under the
[[http://creativecommons.org/licenses/by-sa/3.0/us/][Creative Commons BY-SA license]]. The game also includes accessibility
support for users with red/green color blindness.

*** About the author

David O'Toole is a Boston-area indie developer who makes games with
Common Lisp. Since 2008 he has released several free games for the PC
platform; the most notable among them are 2x0ng and its predecessor,
entitled Xong.

*** Links and contact information

 - 2x0ng on Desura: http://www.desura.com/games/2x0ng
 - 2x0ng homepage: http://blocky.io/2x0ng.html
 - trailer video: http://blocky.io/2x0ng-trailer-4.webm
 - media kit: http://blocky.io/2x0ng-media.html
 - soundtrack: [[https://soundcloud.com/david-otoole-2/sets/2x0ng-the-official-soundtrack][listen now on Soundcloud]]
 - developer's homepage: http://dto.github.io/notebook/
 - developer's email: [[mailto:dto@blocky.io][dto@blocky.io]]
 - developer's twitter: http://twitter.com/dto1138
